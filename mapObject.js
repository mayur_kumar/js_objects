function mapObject(obj, cb) {
    let mapObj={}
    for (const key in obj){
        let val = obj[key]
        mapObj[key]=cb(val)
    }
    return mapObj
}

export{mapObject}